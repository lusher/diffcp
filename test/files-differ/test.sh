#/usr/bin/env sh
set -x

#the existing file is truncated to the first paragraph, being differenct it should be overriden

testname='files-differ'
input="$srcdir/test/$testname/input.txt"
result="$PWD/test/$testname/result.txt"

head -n 1 "$input" > "$result"
cat "$input" | ./diffcp "$result"

diff "$input" "$result"

