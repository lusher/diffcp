#/usr/bin/env sh
set -x

#the existing file is truncated to the first paragraph, being differenct it should be overriden

testname='files-differ'
input="$srcdir/test/$testname/input.txt"
result="$PWD/test/$testname/result.txt"

cat ../test/files-differ/input.txt | valgrind --leak-check=full --error-exitcode=1 ./diffcp output.txt

