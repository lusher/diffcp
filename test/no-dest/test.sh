#/usr/bin/env sh
set -x

#with no destination file the source should be copied as is

testname='no-dest'
result="$PWD/test/$testname/result.txt"
input="$srcdir/test/$testname/input.txt"

rm -f "$result"
cat "$input" | ./diffcp "$result"

diff "$input" "$result"

