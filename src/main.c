#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <openssl/md5.h>

size_t calc_md5(FILE* dest_file, unsigned char *md5_result, char **content_buf, size_t content_buf_len) {
	size_t input_offset = 0;
	char read_buffer[1024];
	MD5_CTX c;
	MD5_Init(&c);
	while (!feof(dest_file)) {
		int len = fread(&read_buffer, 1, sizeof(read_buffer), dest_file);
		//if content_buf is supplied then cop
		if (content_buf != NULL) {
			while ((input_offset + len) > content_buf_len) {  
				content_buf_len *= 2; *content_buf = realloc(*content_buf, content_buf_len);
			}
			memcpy(*content_buf + input_offset, &read_buffer, len);
			input_offset += len;
		}

		MD5_Update(&c, &read_buffer, len);
	}
	MD5_Final(md5_result, &c);
	return input_offset;
}

int main(int argc, char** argv) {
	if (argc < 2) {
		fprintf(stderr, "no file supplied\n");
		return 1;
	}
	
	unsigned char source_md5[MD5_DIGEST_LENGTH] = { 0 };
	char* content_buf = malloc(1024);
	size_t content_buf_len = calc_md5(stdin, &source_md5[0], &content_buf, 1024);

	unsigned char dest_md5[MD5_DIGEST_LENGTH] = { 0 };
	if (access(argv[0], F_OK)) {
		FILE *dest_file = fopen(argv[1], "r+");
		calc_md5(dest_file, &dest_md5[0], NULL, 0);
		fclose(dest_file);
	}

	fprintf(stderr, "source: ");
	for(int n=0; n<MD5_DIGEST_LENGTH; n++)
		fprintf(stderr, "%02x", source_md5[n]);
	fprintf(stderr, "\n");
	fprintf(stderr, "dest:   ");
	for(int n=0; n<MD5_DIGEST_LENGTH; n++)
		fprintf(stderr, "%02x", dest_md5[n]);
	fprintf(stderr, "\n");

	if (0 != memcmp(&source_md5, &dest_md5, MD5_DIGEST_LENGTH)) {
		fprintf(stderr, "files differ\n");
		FILE *dest_file = fopen(argv[1], "w");
		//fseek(dest_file, 0, SEEK_SET);
		fwrite(content_buf, content_buf_len, 1, dest_file);
		fclose(dest_file);
	}

	free(content_buf);
}
